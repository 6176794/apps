import {BsCash, BsCashCoin, BsCashStack, BsSearch, BsShieldFillCheck} from 'react-icons/bs';
import {Ri4KFill, RiCactusLine, RiCakeFill, RiHeart2Fill, RiSpace, RiSpaceShipFill, RiStarFill} from "react-icons/all";

const ServiceCard = ({color,title,icon,subtitle}) =>(
    <div className="flex flex-row justify-start items-center blue-glassmorphism p-3 m-4 ">
        <div className={`w-10 h-10 rounded-full flex justify-center items-center ${color}`}>
            {icon}
        </div>
        <div className="ml-5 flex flex-col flex-1">
                <h1 className="mt-2 text-black text-lg">{title}</h1>
                <p className="mt-2 text-black text-sm md:w-9/12">{subtitle}</p>
        </div>
    </div>
)

const Services = () =>{
    return(
        <div className="flex-row w-full justify-center items-center gradient-bg-services">
            <div className="flex mf:flex-col flex-col items-center justify-between md:p-20 py-12 px-4">
                <div className="flex-1 flex flex-col justify-center items-center">
                    <h1 className="text-white text-3xl sm:text-5xl py-2 text-gradient ">
                        Services that we
                        <br />
                        continue to improve

                    </h1>
                    <p className="text-left my-2 text-white font-light md:w-9/12 w-11/12 text-center mt-5 mb-5 " >
                        The best choice for buying and selling your crypto assets, with the
                        various super friendly services we offer
                    </p>
                </div>

                <div className="flex-1 flex flex-col justify-start items-center ">
                    <ServiceCard
                        color="bg-[#282c34]"
                        title="Security gurantee"
                        icon={<BsShieldFillCheck fontSize={21} className="text-white" />}
                        subtitle="Security is guranteed. We always maintain privacy and maintain the quality of our products"
                    />
                    <ServiceCard
                        color="bg-[#282c34]"
                        title="Best exchange rates"
                        icon={<BsCashStack fontSize={21} className="text-white" />}
                        subtitle="Security is guranteed. We always maintain privacy and maintain the quality of our products"
                    />
                    <ServiceCard
                        color="bg-[#282c34]"
                        title="Fastest transactions"
                        icon={<RiSpaceShipFill fontSize={21} className="text-white" />}
                        subtitle="Security is guranteed. We always maintain privacy and maintain the quality of our products"
                    />
                </div>
            </div>
        </div>
    );
}

export default Services
