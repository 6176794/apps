import React,{useContext} from 'react'
import {TransactionContext} from "../context/TransactionContext";
import {shortenAddress} from "../utils/shortenAddress";
import {trailingComma} from "tailwindcss/prettier.config";
const TransactionCard = ({addressTo,addressFrom,timestamp,message,keyword,amount,url}) =>{




    return (
    <div className="m-4 flex flex-1 blue-glassmorphism
    2xl:min-w-[450px]
    2xl:max-w-[500px]
    sm:min-w-[270px]
    sm:max-w-[300px]
    flex-col p-3 rounded-md hover:shadow-2xl">
        <div className="flex flex-col items-center w-full mt-3">
            <div className="display-flex justify-start w-full mb-6 p-2">
                <a href={`https://ropsten.etherscan.io/address/${addressFrom}`}>
                    <p className="text-black text-base">From: {shortenAddress(addressFrom)}</p>
                </a>
                <a href={`https://ropsten.etherscan.io/address/${addressTo}`}>
                    <p className="text-black text-base">To: {shortenAddress(addressTo)}</p>
                </a>
                <p className="text-black text-vase">Amount:{amount} ETH</p>
                {message && (
                    <>
                    <br/>
                        <p className="text-black text-base">Message:{message}</p>
                    </>
                )}

                <img src="https://media3.giphy.com/media/L59aKIC2MFyfUfrz3n/giphy.gif?cid=ecf05e47mtwfxd6kyz8afkmsgb04c0tuwvqd24l8hnq55oa6&rid=giphy.gif&ct=g"
                        alt="gif"
                        className="w-full h-64 2x:h-96 rounded-md shadow-lg object-cover"
                        />
                
                <div className="bg-black p-4 px-5 w-max mt-5 rounded-3xl shadow-2xl">
                    <p className="text-white font-bold">{timestamp}</p>
                </div>
            </div>
        </div>
    </div>
)
}

const Transactions = () =>{
    const {currentAccount,transactions} = useContext(TransactionContext)
    return(
        <div className="flex w-full justify-center items-center 2xl:px-20 gradient-bg-transactions">
            <div className="flex flex-col md:p-12 py-12 px-4">
                {currentAccount !=' ' ? (
                    <h3 className="text-white text-center my-2">
                        Latest Transactions
                    </h3>
                ) : <h3 className="text-white text-center my-2">
                    Connect Meta Mask 🦊 to your account , send transaction and update page in order to see it
                </h3>}

                <div className="flex flex-wrap justify-center items-center mt-10">
                    {transactions.reverse().map((transaction,i) => (
                        <TransactionCard key={i} {...transaction}/>
                    ))}
                </div>

            </div>
        </div>
    );
}

export default Transactions
