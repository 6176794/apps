//https://eth-ropsten.alchemyapi.io/v2/RghbbbDXzD4F2W4i8uq-GqCRJ-dhaRgf

require('@nomiclabs/hardhat-waffle')

module.exports = {
  solidity: '0.8.0',
  networks:{
    ropsten:{
      url:'https://eth-ropsten.alchemyapi.io/v2/RghbbbDXzD4F2W4i8uq-GqCRJ-dhaRgf',
      accounts:['11a574fc73296465fb166d97bc4becd24cf83dbc66966a2ab2567d3921f47a44']
    }
  }
}
